<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test Lyracons</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="./assets/css/main.css">
</head>

<?php 

$sub_menu = array(
    array(
        'titulo' => 'Ropa',
        'url' => '/ropa.html
    '),
    array(
        'titulo' =>'Electronica',
        'url' => '/electronica.html'
    ),
    array(
        'titulo' => 'Higiene',
        'url' =>'/higiene.html'
    ),
    array(
        'titulo' => 'Alimentos',
        'url' => '/alimentos.html'
    ),
    array(
        'titulo' => 'Otros',
        'url' => '/otros.html'
    )
);

?>

<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="logo-banner col-12">
                    <div class="logo-rectangle"></div>
                </div>
            </div>
            <div class="row">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a id="inicio" class="nav-link" href="#">INICIO <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PRODUCTOS</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <?php
                                foreach ($sub_menu as $node){
                                    echo "<li class='dropdown-item' ><a href='" . $node['url'] . "'>" . $node['titulo'] . "</a></li>";
                                }
                            ?>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a id="nosotros" class="nav-link" href="#" >NOSOTROS</a>
                        </li>
                        <li class="nav-item">
                            <a id="compras" class="nav-link" href="#" >COMPRAS</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <main>
        <div class="container">
            <div class="row">
                <aside class="col-3"></aside>
                <section class="col-9">
                    <div class="row">
                        <div class="col"></div>
                        <div class="col"></div>
                        <div class="col"></div>
                    </div>
                    <div class="row">
                        <div class="col"></div>
                        <div class="col"></div>
                        <div class="col"></div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs="
    crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script>
        $('#inicio').on('click', () => {
            $('main .row .col').css('display', 'none');
        } )

        $('#compras')
        .on('mouseover', () => {
            $('aside').css('background', 'darkred')
        })
        .on('mouseout', () => {
            $('aside').css('background', '#f33827')
        })

        $('#nosotros').on('click', () => {
            $('aside').append('Hola Mundo')
        })
    </script>
</body>
</html>